package com.donnie.spinner;

import java.io.InputStream;

import org.apache.http.util.EncodingUtils;

import com.donnie.spinner.service.MyService;
import com.donnie.spinner.service.MyServiceForData;
import com.donnie.spinner.service.MyServiceForData.MyBinder;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class Service_MainActivity extends Activity {
	
	private Button startService;
	private Button stopService;
	private Button bindService;
	private Button unbindService;
	private Button getData;
	private Button getByIO;
	private Button getAssets;
	
	private Intent intent;
	private Intent intentForData;
	
	private MyBinder binder = null;
	
	public ServiceConnection conn = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			binder = (MyBinder)service;
		}
	};
	
	protected void init(){
		startService = (Button) findViewById(R.id.startService);
		stopService = (Button) findViewById(R.id.stopService);
		bindService = (Button) findViewById(R.id.bindService);
		unbindService = (Button) findViewById(R.id.unbindService);
		getData = (Button) findViewById(R.id.getData);
		getByIO = (Button) findViewById(R.id.raw_info);
		getAssets = (Button) findViewById(R.id.assets_info);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_service__main);
		
		init();
		
		getByIO.setOnClickListener(listener_raw);
		
		getAssets.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String temp = "";
				try {
					InputStream in = getResources().getAssets().open("assert.txt");
					
					int length = in.available();
					
					byte[] buffer = new byte[length];
					
					in.read(buffer);
					temp = EncodingUtils.getString(buffer, "UTF-8");
					in.close();
					Toast.makeText(Service_MainActivity.this, temp.toString(), 1000).show();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		
		
		
		
		intent = new Intent(Service_MainActivity.this,MyService.class);
		intentForData = new Intent(Service_MainActivity.this,MyServiceForData.class);
		
		startService.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startService(intent);
			}
		});
		
		stopService.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				stopService(intent);
			}
		});
		
		bindService.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bindService(intentForData, conn, Service.BIND_AUTO_CREATE);
			}
		});
		
		unbindService.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				unbindService(conn);
			}
		});
		
		getData.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(Service_MainActivity.this, ""+binder.getCount(), 1000).show();
			}
		});
	}
	
	OnClickListener listener_raw = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			String temp = null;
			try {
				InputStream in = getResources().openRawResource(R.raw.test);
				
				int length = in.available();
				
				byte[] buffer = new byte[length];
				
				in.read(buffer);
				
				temp = EncodingUtils.getString(buffer, "UTF-8");
				
				in.close();
				
				Toast.makeText(Service_MainActivity.this, temp.toString(), 1000).show();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.service__main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
