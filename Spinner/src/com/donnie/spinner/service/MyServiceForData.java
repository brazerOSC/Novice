package com.donnie.spinner.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MyServiceForData extends Service{
	
	private int count = 0;
	
	private boolean flags;
	
	MyBinder myBinder = new MyBinder();
	
	public class MyBinder extends Binder{
		public MyBinder(){}
		
		public int getCount(){
			return count;
		}
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.i("MyServiceForData", "onCreate()...创建service...");
		
		new Thread(){
			public void run() {
				super.run();
				while (!flags) {
					count++;
					
				}
			}
		}.start();
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		Log.i("MyServiceForData", "onBind()...绑定service...");
		return myBinder;
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		// TODO Auto-generated method stub
		Log.i("MyServiceForData", "onUnbind()...解绑service...");
		return super.onUnbind(intent);
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i("MyServiceForData", "onDestory()...停止service...");
	}

}
