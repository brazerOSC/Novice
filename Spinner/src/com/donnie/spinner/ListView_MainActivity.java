package com.donnie.spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ListView_MainActivity extends Activity {
	
	private ListView listView;
	private List<String> datalist = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		listView = new ListView(this);
		
		SimpleAdapter adapter = new SimpleAdapter(this, getDate(), R.layout.activity_list_view__main, new String[]{"title","info","img"}, new int[]{R.id.title,R.id.info,R.id.img});
		listView.setAdapter(adapter);
		
		setContentView(listView);
	}
	
	private ArrayList<Map<String, Object>> getDate(){
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("title", "G1");
		map.put("info", "google1");
		map.put("img", R.drawable.ic_launcher);
		list.add(map);
		
		map = new HashMap<String,Object>();
		map.put("title", "G2");
		map.put("info", "google2");
		map.put("img", R.drawable.ic_launcher);
		list.add(map);
		
		map = new HashMap<String,Object>();
		map.put("title", "G3");
		map.put("info", "google3");
		map.put("img", R.drawable.ic_launcher);
		list.add(map);
		
		map = new HashMap<String,Object>();
		map.put("title", "G4");
		map.put("info", "google4");
		map.put("img", R.drawable.ic_launcher);
		list.add(map);
		
		map = new HashMap<String,Object>();
		map.put("title", "G5");
		map.put("info", "google5");
		map.put("img", R.drawable.ic_launcher);
		list.add(map);
		
		map = new HashMap<String,Object>();
		map.put("title", "G6");
		map.put("info", "google6");
		map.put("img", R.drawable.ic_launcher);
		list.add(map);
		return list;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_view__main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
