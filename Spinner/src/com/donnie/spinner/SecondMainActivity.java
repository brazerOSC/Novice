package com.donnie.spinner;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.Toast;

public class SecondMainActivity extends Activity {
	
	private Builder builder = null;
	
	private Button generalBtn,editBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second_main);
		
		builder = new Builder(this);
		
		generalBtn = (Button) findViewById(R.id.general_btn);
		editBtn = (Button)findViewById(R.id.edit_btn);
		
		generalBtn.setOnClickListener(general_listener);
		editBtn.setOnClickListener(edit_listener);
	}
	
	OnClickListener general_listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			builder.setTitle("普通对话框测试");
			
			builder.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
			
			builder.setMessage(getString(R.string.general_dialog_message));
			
			builder.setPositiveButton("confirm[YES]", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(SecondMainActivity.this, "you choose the confirm button !", 1000).show();
				}
			});
			
			builder.setNegativeButton("cancle[NO]", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(SecondMainActivity.this, "you choose the cancel button !", 1000);
				}
			});
			
			builder.create().show();
		}
	};
	
	OnClickListener edit_listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			builder.setTitle("编辑对话框测试");
			
			builder.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
			
			TableLayout tab = (TableLayout)getLayoutInflater().inflate(R.layout.builder_layout, null);
			
			builder.setView(tab);
			
			builder.setPositiveButton("confirm[YES]", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(SecondMainActivity.this, "you choose the confirm button !", 1000).show();
				}
			});
			
			builder.setNegativeButton("cancel[NO]", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(SecondMainActivity.this, "you choose the cancel button !", 1000);
				}
			});
			
			builder.create().show();
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.second_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
