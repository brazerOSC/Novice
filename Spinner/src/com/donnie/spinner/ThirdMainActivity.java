package com.donnie.spinner;

import java.util.ArrayList;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ThirdMainActivity extends Activity {
	
	private ListView listView;
	
	private ArrayList<String> list = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_third_main);
		
		listView = (ListView)findViewById(R.id.listView);
		
		Resources rs = getResources();
		list = new ArrayList<String>();
		list.add(rs.getString(R.string.jack));
		list.add(rs.getString(R.string.peter));
		list.add(rs.getString(R.string.daniel));
		list.add(rs.getString(R.string.lucy));
		list.add(rs.getString(R.string.millie));
		list.add(rs.getString(R.string.facy));
		list.add(rs.getString(R.string.mike));
		list.add(rs.getString(R.string.tone));
		list.add(rs.getString(R.string.bob));
		list.add(rs.getString(R.string.marry));
		list.add(rs.getString(R.string.kitty));
		
		listView.setBackgroundResource(R.color.gray);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,list);
		
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(listview_clickListener);
		listView.setOnItemLongClickListener(listview_longclickListener);
		listView.setOnItemSelectedListener(listview_selectedListener);
		
	}
	
	OnItemClickListener listview_clickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			Toast.makeText(ThirdMainActivity.this, ""+list.get(position), 1000).show();
		}
	};
	
	OnItemLongClickListener listview_longclickListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			Toast.makeText(ThirdMainActivity.this, "长按事件触发", 1000).show();
			return false;
		}
	};
	
	OnItemSelectedListener listview_selectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			Toast.makeText(ThirdMainActivity.this, "您选的条目id是:"+position, 1000).show();
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.third_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
