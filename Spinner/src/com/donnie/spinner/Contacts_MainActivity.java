package com.donnie.spinner;

import com.donnie.spinner.util.DBHelper;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class Contacts_MainActivity extends Activity {
	
	private EditText edit_age,edit_name;
	private DBHelper dbHelper;
	private Button selectBtn,insertBtn;
	private ListView listView;
	private Cursor cursorTemp;
	private String nameTemp,ageTemp;
	
	public void init(){
		selectBtn = (Button) findViewById(R.id.selectBtn);
		insertBtn = (Button) findViewById(R.id.insertBtn);
		listView = (ListView) 
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_friends);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contacts__main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
