package com.donnie.spinner;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class BuilderMainActivity extends Activity {
	
	private Builder builder = null;
	
	private Button list_dialog_btn = null;
	
	private String temp_professional = null;
	
	private TextView tv_showinfo;
	
	private int temp_sum = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.muti_builder);
		
		builder = new Builder(this);
		
		tv_showinfo = (TextView) findViewById(R.id.show_info);
		list_dialog_btn = (Button) findViewById(R.id.click_btn);
		
		//list_dialog_btn.setOnClickListener(listener);
		list_dialog_btn.setOnClickListener(listener_muti);
	}
	
	OnClickListener listener_muti = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			builder.setTitle("带有复选框的列表对话框测试");
			
			String[] arrStrings = getResources().getStringArray(R.array.arr_happy);
			builder.setMultiChoiceItems(arrStrings, null, new OnMultiChoiceClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which, boolean isChecked) {
					// TODO Auto-generated method stub
					if (isChecked) {
						++temp_sum;
					}else {
						--temp_sum;
					}
				}
			});
			
			builder.setPositiveButton("OK"+"", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(BuilderMainActivity.this, temp_sum+"", 1000).show();
				}
			});
			
			builder.create().show();
		}
	};
	
	OnClickListener text_radio_dialog_listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			builder.setTitle(getResources().getString(R.string.professional));
			
			builder.setSingleChoiceItems(getResources().getStringArray(R.array.arr_professinal), 0, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					temp_professional = getResources().getStringArray(R.array.arr_professinal)[which];
					Toast.makeText(BuilderMainActivity.this, "您选的专业是:"+temp_professional, 2000).show();
				}
			});
			
			builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});
			
			builder.create().show();
		}
	};
	
	OnClickListener listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			String[] arrStrings = getResources().getStringArray(R.array.arr_professinal);
			builder.setTitle(getResources().getString(R.string.professional));
			builder.setIcon(R.drawable.ic_launcher);
			
			builder.setItems(arrStrings, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					String temp = getResources().getStringArray(R.array.arr_professinal)[which];
					Toast.makeText(BuilderMainActivity.this, "你选择的专业是："+temp, 1000).show();
				}
			});
			
			builder.create().show();
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.builder_main, menu);
		menu.add(Menu.NONE,0,1,"menu1");
		menu.add(Menu.NONE,1,2,"menu2");
		menu.add(Menu.NONE,2,3,"menu3");
		menu.add(Menu.NONE,3,4,"menu4");
		menu.add(Menu.NONE,4,5,"menu5");
		menu.add(Menu.NONE,5,6,"menu6");
		menu.add(Menu.NONE,6,7,"menu7");
		menu.add(Menu.NONE,7,8,"menu8");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		switch (id) {
		case 0:
			Toast.makeText(BuilderMainActivity.this, "menu1", 1000).show();
			break;
		case 1:
			Toast.makeText(BuilderMainActivity.this, "menu2", 1000).show();
			break;
		case 2:
			Toast.makeText(BuilderMainActivity.this, "menu3", 1000).show();
			break;
		default:
			break;
		}
		
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
